FROM gitlab-registry.cern.ch/linuxsupport/cc7-base

RUN yum -y update
RUN yum install -y epel-release python-pip wget && yum clean all

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN wget https://bootstrap.pypa.io/pip/2.7/get-pip.py
RUN python get-pip.py
RUN pip install --upgrade setuptools

RUN pip install --upgrade pip
RUN pip install --no-cache-dir pyyaml pylint pytest coverage
